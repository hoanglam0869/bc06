function renderDSSV(dssv) {
  var contentHTML = "";
  for (var i = 0; i < dssv.length; i++) {
    const item = dssv[i];
    var contentTr = `<tr>
          <td>${item.ma}</td>
          <td>${item.ten}</td>
          <td>${item.email}</td>
          <td>${(item.toan + item.ly + item.hoa) / 3}</td>
          <td><button class="btn btn-danger" onclick="xoaSV('${
            item.ma
          }')">Xóa</button>
          <button class="btn btn-warning" onclick="suaSV('${
            item.ma
          }')"><a href="#formQLSV">Sửa</a></button></td>
      </tr>`;
    contentHTML += contentTr;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function layThongTinTuForm() {
  var ma = document.getElementById("txtMaSV").value;
  var ten = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var matKhau = document.getElementById("txtPass").value;
  var toan = document.getElementById("txtDiemToan").value * 1;
  var ly = document.getElementById("txtDiemLy").value * 1;
  var hoa = document.getElementById("txtDiemHoa").value * 1;

  return {
    ma: ma,
    ten: ten,
    email: email,
    matKhau: matKhau,
    toan: toan,
    ly: ly,
    hoa: hoa,
  };
}
