// thêm sv

var dssv = [];
const DSSV_LOCAL = "DSSV_LOCAL";
// khi user load trang => lấy dữ liệu từ localStorage
var jsonData = localStorage.getItem(DSSV_LOCAL);
if (jsonData != null) {
  dssv = JSON.parse(jsonData);
  renderDSSV(dssv);
}

function themSV() {
  // lấy thông tin từ form
  var sv = layThongTinTuForm();
  // push(): thêm phần tử vào array
  dssv.push(sv);

  // convert data
  var dataJson = JSON.stringify(dssv);
  // lưu vào localStorage
  localStorage.setItem(DSSV_LOCAL, dataJson);

  // render dssv lên table
  renderDSSV(dssv);
  // tbodySinhVien
  resetForm();
}

function xoaSV(id) {
  // splice: cut, slice: copy
  var viTri = -1;
  for (var i = 0; i < dssv.length; i++) {
    if (dssv[i].ma == id) {
      viTri = i;
    }
  }
  if (viTri != -1) {
    // splice(vị trí, số lượng)
    dssv.splice(viTri, 1);
    renderDSSV(dssv);
  }
}

function suaSV(id) {
  var viTri = dssv.findIndex(function (item) {
    return item.ma == id;
  });
  // show thông tin lên form
  var sv = dssv[viTri];
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.matKhau;
  document.getElementById("txtDiemToan").value = sv.toan;
  document.getElementById("txtDiemLy").value = sv.ly;
  document.getElementById("txtDiemHoa").value = sv.hoa;
}

function capNhatSV() {
  // layThongTinTuForm() => return object sv
  var sv = layThongTinTuForm();
  var viTri = dssv.findIndex(function (item) {
    return item.ma == sv.ma;
  });
  dssv[viTri] = sv;
  renderDSSV(dssv);
}

function resetForm() {
  document.getElementById("formQLSV").reset();
}

// splice, findIndex, map, push, forEach
// localStorage: lưu trữ, JSON: convert data
